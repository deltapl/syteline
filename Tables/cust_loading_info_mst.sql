USE [SytelineDeltaPlastics_App]
GO

/****** Object:  Table [dbo].[cust_loading_info_mst]    Script Date: 05/08/2018 08:43:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[cust_loading_info_mst](
	[site_ref] [dbo].[SiteType] NOT NULL,
	[cust_num] [nvarchar](7) NULL,
	[cust_num_sequence] [int] NOT NULL,
	[carrier_restraints] [nvarchar](255) NULL,
	[loading_dunnage_bag] [tinyint] NULL,
	[loading_no_double_stack] [tinyint] NULL,
	[loading_load_straight] [tinyint] NULL,
	[loading_no_pyramid] [tinyint] NULL,
	[loading_corner_boards] [tinyint] NULL,
	[loading_straps] [tinyint] NULL,
	[loading_loading_position] [int] NULL,
	[unloading_has_dock] [tinyint] NULL,
	[unloading_no_dock] [tinyint] NULL,
	[unloading_chain] [tinyint] NULL,
	[unloading_has_forklift] [tinyint] NULL,
	[unloading_palletJack_only] [tinyint] NULL,
	[unloading_need_liftgate] [tinyint] NULL,
	[unloading_need_palletJack] [tinyint] NULL,
	[unloading_isResidential] [tinyint] NULL,
	[unloading_driverAssist] [tinyint] NULL,
	[unloading_comment] [nvarchar](255) NULL,
	[warehouse_constraint] [nvarchar](255) NULL,
	[delivery_apt_info] [nvarchar](255) NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[InWorkflow] [dbo].[FlagNyType] NOT NULL,
	[mileage_from_warehouse] [int] NULL,
 CONSTRAINT [IX_cust_loading_info_mst_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [pk_cust_loading_info_mst] UNIQUE NONCLUSTERED 
(
	[site_ref] ASC,
	[cust_num] ASC,
	[cust_num_sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[cust_loading_info_mst] ADD  CONSTRAINT [DF_cust_loading_info_mst_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO

ALTER TABLE [dbo].[cust_loading_info_mst] ADD  CONSTRAINT [DF_cust_loading_info_mst_UpdatedBy]  DEFAULT (suser_sname()) FOR [UpdatedBy]
GO

ALTER TABLE [dbo].[cust_loading_info_mst] ADD  CONSTRAINT [DF_cust_loading_info_mst_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[cust_loading_info_mst] ADD  CONSTRAINT [DF_cust_loading_info_mst_RecordDate]  DEFAULT (getdate()) FOR [RecordDate]
GO

ALTER TABLE [dbo].[cust_loading_info_mst] ADD  CONSTRAINT [DF_cust_loading_info_mst_RowPointer]  DEFAULT (newid()) FOR [RowPointer]
GO

ALTER TABLE [dbo].[cust_loading_info_mst] ADD  CONSTRAINT [DF_cust_loading_info_mst_NoteExistsFlag]  DEFAULT ((0)) FOR [NoteExistsFlag]
GO

ALTER TABLE [dbo].[cust_loading_info_mst] ADD  CONSTRAINT [DF_cust_loading_info_mst_InWorkflow]  DEFAULT ((0)) FOR [InWorkflow]
GO


