USE [SytelineDeltaPlastics_App]
GO

/****** Object:  Table [dbo].[co_additional_info_mst]    Script Date: 05/08/2018 10:45:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[co_additional_info_mst](
	[site_ref] [dbo].[SiteGroupType] NOT NULL,
	[co_num] [nvarchar](10) NOT NULL,
	[orderReceived_datetime] [datetime] NULL,
	[deliveryCommitment_date] [date] NULL,
	[orderVerified_datetime] [datetime] NULL,
	[verification_issue] [nvarchar](255) NULL,
	[production_issue] [nvarchar](255) NULL,
	[transferOrder_number] [nvarchar](50) NULL,
	[secondRequest_date] [date] NULL,
	[readyToShip_date] [date] NULL,
	[backOrder_flag] [tinyint] NULL,
	[backOrder_justification] [nvarchar](255) NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[InWorkflow] [dbo].[FlagNyType] NOT NULL,
 CONSTRAINT [PK_co_additional_info_mst] PRIMARY KEY CLUSTERED 
(
	[site_ref] ASC,
	[co_num] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_co_additional_info_mst_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[co_additional_info_mst] ADD  CONSTRAINT [DF_co_additional_info_mst_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO

ALTER TABLE [dbo].[co_additional_info_mst] ADD  CONSTRAINT [DF_co_additional_info_mst_UpdatedBy]  DEFAULT (suser_sname()) FOR [UpdatedBy]
GO

ALTER TABLE [dbo].[co_additional_info_mst] ADD  CONSTRAINT [DF_co_additional_info_mst_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[co_additional_info_mst] ADD  CONSTRAINT [DF_co_additional_info_mst_RecordDate]  DEFAULT (getdate()) FOR [RecordDate]
GO

ALTER TABLE [dbo].[co_additional_info_mst] ADD  CONSTRAINT [DF_co_additional_info_mst_RowPointer]  DEFAULT (newid()) FOR [RowPointer]
GO

ALTER TABLE [dbo].[co_additional_info_mst] ADD  CONSTRAINT [DF_co_additional_info_mst_NoteExistsFlag]  DEFAULT ((0)) FOR [NoteExistsFlag]
GO

ALTER TABLE [dbo].[co_additional_info_mst] ADD  CONSTRAINT [DF_co_additional_info_mst_InWorkflow]  DEFAULT ((0)) FOR [InWorkflow]
GO


