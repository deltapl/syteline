USE [SytelineDeltaPlastics_App]
GO

/****** Object:  Table [dbo].[co_freight_info_mst]    Script Date: 05/08/2018 10:27:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[co_freight_info_mst](
	[site_ref] [dbo].[SiteType] NOT NULL,
	[co_num] [dbo].[CoNumType] NOT NULL,
	[freight_schedule_reason] [nvarchar](255) NULL,
	[shipping_issue] [nvarchar](255) NULL,
	[cust_request_deliv_date] [date] NULL,
	[cust_doNot_ship_before] [date] NULL,
	[cpu_date_notified] [date] NULL,
	[carrier_name] [nvarchar](60) NULL,
	[carrier_tracking_info] [nvarchar](50) NULL,
	[freight_quote_cost] [decimal](8, 2) NULL,
	[poReq_freight] [nvarchar](50) NULL,
	[freight_po] [nvarchar](50) NULL,
	[scheduled_ship_date] [date] NULL,
	[est_delivery_date] [date] NULL,
	[driver_checkIN] [datetime] NULL,
	[actual_ship_date] [datetime] NULL,
	[proof_delivery_timestamp] [datetime] NULL,
	[secondary_ship_info] [nvarchar](255) NULL,
	[actual_mileage] [int] NULL,
	[combined_mileage] [int] NULL,
	[distance_origin] [int] NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[InWorkflow] [dbo].[FlagNyType] NOT NULL,
	[cust_num] [dbo].[CustNumType] NULL,
	[cust_seq] [dbo].[CustSeqType] NULL,
	[freight_budget] [decimal](10, 2) NULL,
 CONSTRAINT [PK_co_freight_info_mst] PRIMARY KEY CLUSTERED 
(
	[site_ref] ASC,
	[co_num] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_co_freight_info_mst_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[co_freight_info_mst] ADD  CONSTRAINT [DF_co_freight_info_mst_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO

ALTER TABLE [dbo].[co_freight_info_mst] ADD  CONSTRAINT [DF_co_freight_info_mst_UpdatedBy]  DEFAULT (suser_sname()) FOR [UpdatedBy]
GO

ALTER TABLE [dbo].[co_freight_info_mst] ADD  CONSTRAINT [DF_co_freight_info_mst_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[co_freight_info_mst] ADD  CONSTRAINT [DF_co_freight_info_mst_RecordDate]  DEFAULT (getdate()) FOR [RecordDate]
GO

ALTER TABLE [dbo].[co_freight_info_mst] ADD  CONSTRAINT [DF_co_freight_info_mst_RowPointer]  DEFAULT (newid()) FOR [RowPointer]
GO

ALTER TABLE [dbo].[co_freight_info_mst] ADD  CONSTRAINT [DF_co_freight_info_mst_NoteExistsFlag]  DEFAULT ((0)) FOR [NoteExistsFlag]
GO

ALTER TABLE [dbo].[co_freight_info_mst] ADD  CONSTRAINT [DF_co_freight_info_mst_InWorkflow]  DEFAULT ((0)) FOR [InWorkflow]
GO


