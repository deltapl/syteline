USE [SytelineDeltaPlastics_App]
GO

/****** Object:  Trigger [co_additional_info_mstInsert]    Script Date: 05/08/2018 10:46:21 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[co_additional_info_mstInsert]'))
DROP TRIGGER [dbo].[co_additional_info_mstInsert]
GO

USE [SytelineDeltaPlastics_App]
GO

/****** Object:  Trigger [dbo].[co_additional_info_mstInsert]    Script Date: 05/08/2018 10:46:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[co_additional_info_mstInsert]
ON [dbo].[co_additional_info_mst]
INSTEAD OF INSERT
AS
   IF @@ROWCOUNT = 0 RETURN
   
   SET NOCOUNT ON

   DECLARE @Today DateType
   SET @Today = dbo.GetSiteDate(GETDATE())

   DECLARE @UserName LongListType
   SET @UserName = dbo.UserNameSp()
    
    declare @paddedCo nvarchar(10)    
    set @paddedCo = (
		select 
			isnull(right(space(9) + co_num,10),co_num)
		from
			inserted ii
		)
		

   INSERT [co_additional_info_mst] (
      [site_ref]
      , [co_num]
      , [orderReceived_datetime]
      , [deliveryCommitment_date]
      , [orderVerified_datetime]
      , [verification_issue]
      , [production_issue]
      , [transferOrder_number]
      , [secondRequest_date]
      , [readyToShip_date]
      , [backOrder_flag]
      , [backOrder_justification]
      , [CreatedBy]
      , [UpdatedBy]
      , [CreateDate]
      , [RecordDate]
      , [RowPointer]
      , [NoteExistsFlag]
      , [InWorkflow]
      )
   SELECT
      [site_ref]
      , @paddedCo
      , [orderReceived_datetime]
      , [deliveryCommitment_date]
      , [orderVerified_datetime]
      , [verification_issue]
      , [production_issue]
      , [transferOrder_number]
      , [secondRequest_date]
      , [readyToShip_date]
      , [backOrder_flag]
      , [backOrder_justification]
      , @Username
      , @Username
      , @Today
      , @Today
      , [RowPointer]
      , [NoteExistsFlag]
      , [InWorkflow]
   FROM inserted bt

   -- The AFTER INSERT Triggers fire now, in the following order:
   --   co_additional_info_mstIup First (if exists; manually maintained)
   --   co_additional_info_mstInsAudit and/or any custom triggers (if exist; generated by AuditLoggingGenCodeSp and/or manually maintained)
   --   co_additional_info_mstIupReplicate Last (if exists; generated by ReplicationTriggerIupCodeSp)

   RETURN
GO


